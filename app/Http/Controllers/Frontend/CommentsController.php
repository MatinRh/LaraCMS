<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    public function store(Request $request, $id)
    {
        $post = Post::published()->find($id);
        if (is_null($post)) {
            abort(404);
        }
        $newCommentResult = $post->comments()->create([
            'user_id' => Auth::id(),
            'body'    => $request->input('comment_content'),
            'status'  => Comment::MODERATED,
        ]);
        if($newCommentResult &&  $newCommentResult instanceof Comment)
        {
            return redirect()->back()->with('status','دیدگاه شما با موفقیت ثبت گردید.');
        }
    }
}
