<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostsController extends Controller
{

    public function show(Request $request, $slug, $id)
    {
        $post = Post::published()->find($id);
        if (is_null($post)) {
            abort(404);
        }
        $comments = Comment::approved()->get();
        return view('frontend.articles.show', compact('post','comments'));
    }
}
