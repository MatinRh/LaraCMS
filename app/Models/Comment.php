<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    const MODERATED = 0;
    const APPROVED = 1;


    protected $fillable = [
        'user_id',
        'body',
        'parent_id',
        'status'
    ];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function scopeApproved($query)
    {
        return $query->where('status',Comment::APPROVED);
    }
}
